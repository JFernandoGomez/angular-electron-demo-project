import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {NgxElectronModule} from 'ngx-electron';
import {APP_ROUTES} from './appRoutes';
import {ALL_COMPONENTS, BOOTSTRAP_COMPONENT} from './components/all';
import {ALL_SERVICES} from './services/all';
import { Http, HttpModule, XHRBackend, RequestOptions } from '@angular/http';

import { FormsModule }   from '@angular/forms';
import { MaterialModule } from './components/material/material.module';
import { DialogComponent } from './components/dialog/dialog.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        CommonModule,
        NgxElectronModule,
        NoopAnimationsModule,
        HttpModule,
        MaterialModule,
        RouterModule.forRoot(APP_ROUTES, { useHash: true })
    ],
    declarations: [...ALL_COMPONENTS],
    bootstrap: [BOOTSTRAP_COMPONENT],
    providers: [
        ...ALL_SERVICES
    ],
    entryComponents: [DialogComponent]
})
export class AppModule {

}
