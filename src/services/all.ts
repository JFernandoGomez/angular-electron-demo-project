import {NotificationService} from './notificationService';
import {LoginService} from './login.service';
import {UserService} from './user.service';
import {OperationsService} from './operations.service';


export const ALL_SERVICES: Array<any> = [
    NotificationService,
    LoginService,
    UserService,
    OperationsService
];
