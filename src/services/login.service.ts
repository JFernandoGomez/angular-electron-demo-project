import { Injectable }              from '@angular/core';
import { Http, Response, Headers, RequestOptions }          from '@angular/http';

import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { User } from '../models/user';


@Injectable()
export class LoginService {

  private mockLoginUrl = 'https://reqres.in/api/login';  // URL to web API
  private loginUrl = 'merchantLogin';

  constructor ( private http: Http ) {}

  getUser():Observable<any> {
  	let data = {"email": "peter@klaven","password": "cityslicka"};

  	return this.http.post(this.mockLoginUrl, data)
  	.map( (response: Response) => response.json() );
  }

  doLogin(username:string, password:string):Observable<any>{
    let data =  {
      'Username': username,
      'Password': password,
      'ApiKey': 'zdc307thlspz5zvbamy0g5t15atfvn7v',
      'lang':'en',
      'region':'es_MX',
      'build':'0.0.1',
      'appVersion':'1.0.0',
      'appId':'net.realpayment.win.krispy',
      'deviceId':'74cffc74-639e-11e7-907b-a6006ad3dba0',
      'os':'macOS',
      'osVersion':'10.11.6'
    };

    let headers = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' });
    
    let params  = new URLSearchParams();

    for(let i in data){
      params.append(i,data[i]);
    }

    let options = new RequestOptions({ headers: headers });
    let body = params.toString();

    return this.http.post(this.loginUrl, body , options)
    .map( (response: Response) => response.json() );
  }

  doLoginMock(): Observable<any>{
  	// getUser(){
		let userObj = {
			token : "xyxyxyxyxyxyxyxyxyxxyxyxyxyx",
			name : "Jimmy",
			firstName : "Jimmy Bond",
			
			expire : "2017-06-06",
			userId: "8",
			image: "22",
			fullImageUrl: "https://static.realpayment.net/xxxxxxx",
			"merchantId" : "10020",    
			"merchantLocationId":"0", 
			"merchantName": "Name of merchant",
			"phone": "000-000-0000",
			"email": "user@sample.com",
			pin : "0000"
		};

		let obs = Observable.of( userObj )
		.map ( (response) => {  		
			let newUser = new User(response);
			return newUser;
		});

		return obs;
		
	}

  // getHeroes(): Observable<User[]> {
  // 	return this.http.get(this.heroesUrl)
  // 	.map(this.extractData)
  // 	.catch(this.handleError);
  // }
  // private extractData(res: Response) {
  // 	let body = res.json();
  // 	return body.data || { };
  // }

  // private handleError (error: Response | any) {
  //   // In a real world app, you might use a remote logging infrastructure
  //   let errMsg: string;
  //   if (error instanceof Response) {
  //   	const body = error.json() || '';
  //   	const err = body.error || JSON.stringify(body);
  //   	errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
  //   	} else {
  //   		errMsg = error.message ? error.message : error.toString();
  //   	}
  //   	console.error(errMsg);
  //   	return Observable.throw(errMsg);
  //   }
  // }
}