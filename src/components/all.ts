import { RootComponent } from './root/root';
import { LoginComponent } from './login/login.component';
import { HomeComponent } from './home/home.component';
import { IssueComponent } from './issue/issue.component';
import { RedeemComponent } from './redeem/redeem.component';
import { BalanceComponent } from './balance/balance.component';
import { DialogComponent } from './dialog/dialog.component';

export const ALL_COMPONENTS: Array<any> = [
    RootComponent,
    LoginComponent,
    HomeComponent,
    IssueComponent,
    RedeemComponent,
    BalanceComponent,
    DialogComponent
];

export const BOOTSTRAP_COMPONENT: any = RootComponent;
