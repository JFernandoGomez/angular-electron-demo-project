import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { UserService } from '../../services/user.service';
import { OperationsService } from '../../services/operations.service';

@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.component.html',
  styleUrls: ['./redeem.component.scss']
})
export class RedeemComponent implements OnInit {
	isFormDisabled: Boolean = false;
  isValidating: Boolean = false;
  public user: any = this.userService.getUser();

  public redeemModel = {
    token: '',
    amount: ''
  }

  constructor(
    private router: Router, 
    public snackBar: MdSnackBar,
    private userService: UserService,
    private operationsService: OperationsService
  ) { }

  ngOnInit() {
    this.initializeModel();
  }

  initializeModel(){
    this.redeemModel = {
      token: '',
      amount: ''
    }
  }

  getRandomInt(min, max) {
      return Math.floor(Math.random() * (max - min + 1)) + min;
  }

  goHome(){
    this.router.navigate(["/home"]);
  }

  save(){

  	this.isFormDisabled = true;
    this.isValidating = true;

    this.operationsService.redeemCard(this.redeemModel, this.user)
    .subscribe( response => {
      let r = response;
      console.log('issue response', r );

      if(r.responseCode !== 100 || r.success !== 1){
        console.error('Redeem card error: '+ r.message);
        

        this.isValidating = false;
        this.isFormDisabled = false;
        let snackBarRef = this.snackBar.open(
          'Redeem card error. '+r.message,
          '',
          { 
            duration: 6000,
            extraClasses: ['accent']
          }
        );

      }else{
        this.isValidating = false;
        this.isFormDisabled = false;
        this.initializeModel();
        let snackBarRef = this.snackBar.open('Giftcard redeemed!!','',{duration : 5000});
        snackBarRef.afterDismissed().subscribe(() => {
          
        });
        
      }
    });
  
  }

}
