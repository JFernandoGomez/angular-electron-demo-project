import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  public user: any;
  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    console.log('is user? ',this.userService.isUser()) ;
    this.user = this.userService.getUser();

  	console.log('logging on ngOnInit',this.user);
  }

  issue(){
  	this.router.navigate(['/issue']);
  }

  redeem(){
  	this.router.navigate(['/redeem']);
  }

  balance(){
  	this.router.navigate(['/balance']);
  }
}
