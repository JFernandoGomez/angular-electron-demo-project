import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { MdDialog } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import { UserService } from '../../services/user.service';
import { OperationsService } from '../../services/operations.service';

@Component({
  selector: 'app-issue',
  templateUrl: './issue.component.html',
  styleUrls: ['./issue.component.scss']
})
export class IssueComponent implements OnInit {
	isFormDisabled: Boolean = false;
  isValidating: Boolean = false;
  public user: any = this.userService.getUser();
  public issueModel = {
    userInfo: '',
    amount: '',
    campaignId: '',
    userMessage: '',
    externalReference: ''
  }

  constructor(
    private router: Router, 
    public snackBar: MdSnackBar, 
    public dialog: MdDialog,
    private userService: UserService,
    private operationsService: OperationsService
  ) { 
    
  }


  ngOnInit() {
    this.initializeModel();
  }

  goHome(){
    this.router.navigate(["/home"]);
  }

  initializeModel(){
    this.issueModel = {
      userInfo: '',
      amount: '',
      campaignId: '',
      userMessage: '',
      externalReference: ''
    };
  }

  checkPhone(){
    
    let dialogRef = this.dialog.open(DialogComponent,{
      data: this.issueModel.userInfo
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result=="true"){
        this.save();
      }
    });

  }

  save(){
    console.log(this.issueModel);
    this.isFormDisabled = true;
    this.isValidating = true;

    this.operationsService.issueCard(this.issueModel, this.user)
    .subscribe( response => {
      let r = response;
      console.log('issue response', r );

      if(r.responseCode !== 100 || r.success !== 1){
        console.error('Issue card error: '+ r.message);
        

        this.isValidating = false;
        this.isFormDisabled = false;
        let snackBarRef = this.snackBar.open(
          'Issue card error. '+r.message,
          '',
          { 
            duration: 3000,
            extraClasses: ['accent']
          }
        );

      }else{
        this.isValidating = false;
        this.isFormDisabled = false;
        this.initializeModel();
        let snackBarRef = this.snackBar.open('Giftcard issued!! Token: '+r.token,'',{duration : 8000});
        snackBarRef.afterDismissed().subscribe(() => {
          
        });
        
      }
    });
  	
  	
  	
  }

}


