

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MdButtonModule, MdCheckboxModule, MdListModule, 
	MdInputModule, MdIconModule, MdCardModule, 
	MdToolbarModule, MdSelectModule, MdDialogModule,
	MdProgressBarModule, MdSnackBarModule 
} from '@angular/material';

let modules = [
	MdButtonModule, MdCheckboxModule, MdListModule, 
	MdInputModule, MdIconModule, MdCardModule, 
	MdToolbarModule, MdSelectModule, MdDialogModule,
	MdProgressBarModule, MdSnackBarModule
];

@NgModule({
  imports: [ modules ],
  exports: [ modules ],
})
export class MaterialModule { }