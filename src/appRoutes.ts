import {Routes} from '@angular/router';

import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { IssueComponent } from './components/issue/issue.component';
import { RedeemComponent } from './components/redeem/redeem.component';
import { BalanceComponent } from './components/balance/balance.component';

export const APP_ROUTES: Routes = [
    { 
    	path: '',
    	component: LoginComponent 
    },
    { 
    	path: 'home',
    	component: HomeComponent 
    },
    { 
        path: 'issue',
        component: IssueComponent 
    },
    { 
        path: 'redeem',
        component: RedeemComponent 
    },
    { 
        path: 'balance',
        component: BalanceComponent 
    },

];
